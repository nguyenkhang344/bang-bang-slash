﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordPosition : MonoBehaviour {
    [SerializeField]
    private float speedRotate;
    [SerializeField]
    private float appearTime;
    Transform sword0;
    Transform sword1;
    void Start()
    {
        sword0 = transform.GetChild(0);
        sword1 = transform.GetChild(1);
    }
    void Update()
    {

        transform.Rotate(Vector3.forward * speedRotate);
        if (ButtonInGame.Button.doSlowMo)
        {
            swordDisapper();
            StartCoroutine(swordApper());
        }
     
    }
    IEnumerator swordApper()
    {
        yield return new WaitForSeconds(appearTime);
        sword0.gameObject.SetActive(true);
        sword1.gameObject.SetActive(true);
    }
    void swordDisapper()
    {
        sword0.gameObject.SetActive(false);
        sword1.gameObject.SetActive(false);
    }
}
