﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy1Attack : MonoBehaviour {
    public Transform Player;
    public float DistanceToPlayer;
    public float enemySpeed;
	// Use this for initialization
	void Start () {
        Player = GameObject.Find("Ninja").transform;
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Vector3.Distance(transform.position, Player.transform.position) < DistanceToPlayer)
        {
            transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, enemySpeed);
        }
		
	}
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
           
        }
        
    }
}
