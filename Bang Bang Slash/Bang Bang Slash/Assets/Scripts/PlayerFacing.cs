﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFacing : MonoBehaviour {
    private bool facingRight;
    public Transform Sword;
    // Use this for initialization
    void Start()
    {
        Sword = GameObject.Find("Sword").transform;
    }
    void FixedUpdate () {
        if (transform.position.x < Sword.transform.position.x && facingRight)
        {
            Flip();
        }
        if (transform.position.x >= Sword.transform.position.x && !facingRight)
        {
            Flip();
        }
        if (!Surfing.surfing.Sword.gameObject.activeInHierarchy) Flip();
       
	}
    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = gameObject.transform.localScale;
        theScale.x *= -1;
        gameObject.transform.localScale = theScale;

    }
}
