﻿using UnityEngine;

public class TimeManager : MonoBehaviour {

    public float slowDownFactor = 0.05f;
    public float slowDownLength = 2f;
    void Update()
    {
        Time.timeScale += (1f / slowDownLength)*Time.unscaledDeltaTime;
        //dung Time.unscaledDeltaTime thi khong bi anh huong boi time scale
        Time.timeScale = Mathf.Clamp(Time.timeScale, 0f, 1f);
    }
    public void DoSlowMotion()
    {
        Time.timeScale = slowDownFactor;
        Time.fixedDeltaTime = Time.timeScale * .02f;

    }
}
