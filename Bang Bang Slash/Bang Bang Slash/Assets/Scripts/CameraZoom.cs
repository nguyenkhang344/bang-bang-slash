﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraZoom : MonoBehaviour {
    [SerializeField]
    private float zoom = 1.5f;
    [SerializeField]
    private float normal = 1;
    [SerializeField]
    private float smooth = 30;

    private bool isZoomed = false;
    void Update()
    {
        Zoom();
    }
    void Zoom()
    {
        if (Surfing.surfing.clicked==1)
        {
            isZoomed = true;
        }
        else
        {
            isZoomed = false;
        }
        if (isZoomed)
        {
            GetComponent<Camera>().orthographicSize = Mathf.Lerp(GetComponent<Camera>().orthographicSize, zoom, Time.unscaledDeltaTime* smooth);
        }
        else
        {
            GetComponent<Camera>().orthographicSize = Mathf.Lerp(GetComponent<Camera>().orthographicSize, normal,Time.unscaledDeltaTime* smooth);
        }
    }
}
