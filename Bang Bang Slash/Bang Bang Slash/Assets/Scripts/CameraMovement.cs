﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
    public Transform cameraTarget;

    public float widthDistance;
    public float smoothing;

    Vector3 facing;
    Vector3 offset;
    float facingDir;

    void Start()
    {
        cameraTarget = GameObject.Find("Ninja").transform;
        offset = transform.position - cameraTarget.position;
    }
   
    void FixedUpdate()
    {

        if (Surfing.surfing.clicked == 0)
        {
            facing = new Vector3(widthDistance * 1, 0, 0);


            Vector3 targetCamPos = cameraTarget.position + offset + facing;
            transform.position = Vector3.Lerp(cameraTarget.position, targetCamPos, smoothing);
        }
    }

}
