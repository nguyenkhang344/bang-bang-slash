﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheck : MonoBehaviour {
    public static GroundCheck groundCheck;
    public LayerMask whatIsGround;
    public bool isGround;
    [SerializeField]
    float distanceChecking = .5f;
    float groundRadius = .2f;
    // Use this for initialization
    void Start()
    {
        groundCheck = this;

    }
    void Update() {
       
        isGround= GroundChecking();
      //  Debug.Log(isGround);
    }
    private bool GroundChecking()
    {
        Debug.DrawRay(transform.position, -transform.up.normalized * distanceChecking, Color.blue);
        RaycastHit2D[] hit2D = Physics2D.CircleCastAll(transform.position, groundRadius, -transform.up, distanceChecking, whatIsGround);
        if (hit2D != null)
        {
            foreach (RaycastHit2D hit in hit2D)
            {
                return true;
            }
        } return false;
    } 
}
