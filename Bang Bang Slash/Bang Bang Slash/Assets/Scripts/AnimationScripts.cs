﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationScripts : MonoBehaviour {
    Animator anim;


    void Start()
    {
        anim = gameObject.GetComponent<Animator>();    
    }
    void Update()
    {
        DoThrow();
        DoFLy();
        DoLanding();
        DoFalling();
    }

   private void DoThrow()
    {
        if (!ButtonInGame.Button.canPress)
        {
            anim.SetBool("Throw", true);
        }
        else anim.SetBool("Throw", false);
    }
   private void DoFLy()
    {
        if (Surfing.surfing.release)
        {
            anim.SetBool("Fly", true);
        }
        
    }
    public void DoFalling()
    {
       if(!Surfing.surfing.release) anim.SetBool("Fly", false);


    }
    private void DoLanding()
    {
        if (GroundCheck.groundCheck.isGround)
        {
            anim.SetBool("Land", true);
            anim.SetBool("Falling", false);
        }
        else {
            anim.SetBool("Falling", false);
            anim.SetBool("Land", false);
        }
    }
  
}
