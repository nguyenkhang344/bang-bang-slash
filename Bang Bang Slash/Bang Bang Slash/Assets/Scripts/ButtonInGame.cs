﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonInGame : MonoBehaviour {
    public Button slowMotionButton;
    public static ButtonInGame Button;
    public TimeManager timeManager;
    public GameObject ninja;
    public GameObject sword;
    public bool doSlowMo;
    public bool canPress=true;
    [SerializeField]
    private float SlowMoTimming=0.45f;
    void Start()
    {
        Button = this;
        sword.SetActive(false);
        doSlowMo = false;
    }
    void Update()
    {
        ButtonInteract();
    }
    public void SlowDownButton()
    {
        if (canPress)
        {       
            canPress = false;
            doSlowMo = true;
            timeManager.DoSlowMotion();
            sword.transform.position = new Vector2(ninja.transform.position.x+0.2f, ninja.transform.position.y);
            sword.SetActive(true);
            StartCoroutine(normalTrigger());

        }
    }
    IEnumerator normalTrigger()
    {
        yield return new WaitForSeconds(timeManager.slowDownLength);
        sword.SetActive(false);
        doSlowMo = false;
        Time.timeScale = 1;
        canPress = true;
    }
    private void ButtonInteract()
    {

        if (canPress || Time.timeScale == 1) slowMotionButton.interactable = true;
        else
        {
            slowMotionButton.interactable = false;
        }
    }
}
