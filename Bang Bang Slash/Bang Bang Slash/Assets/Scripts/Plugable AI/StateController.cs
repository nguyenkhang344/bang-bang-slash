﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateController : MonoBehaviour {

    public State curState;
    public State remainSate;

    [HideInInspector]public float stateTimeElapsed;
    void Update()
    {
        curState.UpdateState(this);
    }
    public void TransitionToState(State nextState)
    {
        if(nextState != remainSate)
        {
            curState = nextState;
            OnExitState();
        }

    }
    public bool CheckIfCountDownElapsed(float duration)
    {
        stateTimeElapsed += Time.deltaTime;
        return (stateTimeElapsed >= duration);
        //cach viet tat cua neu stateTimeElapsed lon hon duration thi return true
    }
    private void OnExitState()
    {
        stateTimeElapsed = 0;
    }
}
