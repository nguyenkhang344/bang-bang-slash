﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName=("PluggableAI/State"))]
public class State : ScriptableObject {
    public Transition[] transitions;
    public ActionsAI[] actions;
    public Color sceneGizmoColor = Color.grey;

    public void UpdateState(StateController controller)
    {
        DoAction(controller);
        checkTransitions(controller);
    }
    private void DoAction(StateController controller)
    {
        for(int i=0; i < actions.Length; i++)
        {
            actions[i].Act(controller);
        }
    }
    private void checkTransitions(StateController controller)
    {
        for(int i =0; i < transitions.Length; i++)
        {
            bool decisionSuceeded = transitions[i].decision.Decide(controller);
            if (decisionSuceeded)
            {
                controller.TransitionToState(transitions[i].trueState);
                // neu hoan tat quyet dinh la dung thi truyen transition thu i cua state dung
            }
            else controller.TransitionToState(transitions[i].falseState);
            // ne hoan tat quyet dinh la sai thi truyen transitions thu i thuoc false state
        }
    }
}
