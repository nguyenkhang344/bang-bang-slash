﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateScripts : MonoBehaviour {
    public Transform Player;
    [SerializeField]
    private float rotateSpeed;
    private float plusDegree = -95;
    void Start()
    {
        if (transform == null)
        {
            Debug.LogError("no Player found");
            return;
        }
    }
    void Update()
    {
        Vector2 direction =Player.position - transform.position;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle+(plusDegree), Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation,rotation, rotateSpeed*Time.deltaTime);
    }
}
