﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Surfing : MonoBehaviour {
    public static Surfing surfing;
    public int clicked;
    public Rigidbody2D Sword;
    public Rigidbody2D rb;
    public Transform target;
    public float surfingForce = 20;
    public float slideRange;
    public bool release;

    public float maxDragDistance;
    public bool isPressed;
    
    void Start()
    {
        surfing = this;
    }
    void Update()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (isPressed && ButtonInGame.Button.doSlowMo)
        { 
            target.gameObject.SetActive(true);
            if (Vector3.Distance(mousePos, Sword.position) > maxDragDistance)
            {
                rb.position = Sword.position + (mousePos - Sword.position).normalized * maxDragDistance;
            }
            else
            {
                rb.position = mousePos;
            }
            Vector2 dir = Sword.position-rb.position;
            target.position=Sword.position+dir*slideRange;
            clicked = 1;
           
        }
        if (!isPressed&&clicked==1||clicked==1 && !ButtonInGame.Button.doSlowMo)
        {
            release = true;
            Sword.gameObject.SetActive(false);
            Time.timeScale = 1;
            float step = surfingForce * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position,target.position , maxDistanceDelta:step);
            if (Vector3.Distance(rb.position, target.position) < 0.1)
            {
                clicked = 0;
                ButtonInGame.Button.doSlowMo = false;
                rb.velocity = new Vector2(0, 0);
                target.gameObject.SetActive(false);
                release = false;
                //Button
                ButtonInGame.Button.canPress = true;
            }

        }
        if (clicked == 0)
        {
            rb.isKinematic = false;
            
        }
    }
    void OnMouseDown()
    {
        isPressed = true;
        rb.isKinematic = true;
      
    }
    void OnMouseUp()
    {
        isPressed = false;
       
    }
}

